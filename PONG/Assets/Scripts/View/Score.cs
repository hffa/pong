using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [Tooltip("Placar a ser atualizado")]
    public Text[] scoreText;
    [Tooltip("Pontos")]
    int[] score = new int[2];

    private static Score instance;
    public static Score Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Score>();
            }

            return instance;
        }
    }

    internal void UpdateScore(int goalId)
    {
        score[goalId]++;
        scoreText[goalId].text = score[goalId].ToString();
    }
}
