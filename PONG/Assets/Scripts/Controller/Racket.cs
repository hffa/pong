using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racket : MonoBehaviour
{
    private static Racket instance;
    public static Racket Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Racket>();
            }

            return instance;
        }
    }

    [Tooltip("Raquetes")]
    public Transform racketLeft, racketRight;

    Vector3 originL, originR;

    void Start()
    {
        originL = racketLeft.position;
        originR = racketRight.position;
    }

    internal void ResetRacket()
    {
        racketLeft.position = originL;
        racketRight.position = originR;
    }
}
