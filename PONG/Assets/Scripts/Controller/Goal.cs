﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour {

    public int goalId;

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "Ball")
        {
            Score.Instance.UpdateScore(goalId);
            Racket.Instance.ResetRacket();
        }
    }
}
