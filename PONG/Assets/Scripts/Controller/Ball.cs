﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour {

    [Tooltip("Velocidade da bolinha.")]
    public float speed = 240;
    [Tooltip("Ultimo lugar onde a bolinha colidiu")]
    int lastHit; //0 left <, 1 right>
    [Tooltip("Audio - Colisão wall")]
    public AudioSource audioWall;
    [Tooltip("Audio - Colisão raquete")]
    public AudioSource audioRacket;

    void Start()
    {
        StartMatch();
    }

    /// <summary>
    /// Detecta onde foi a colisão da bolinha.
    /// 1 = topo da raquete, 0 = meio da raquete, -1 = baixo da raquete
    /// </summary>
    /// <param name="ballPos"></param>
    /// <param name="racketPos"></param>
    /// <param name="racketHeight"></param>
    /// <returns></returns>
    float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight)
    {
        return (ballPos.y - racketPos.y) / racketHeight;
    }

    /// <summary>
    /// Ao colidir com uma raquete, joga a bolinha na direção oposta a direção de origem.
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionEnter2D(Collision2D col)
    {
        //left
        if (col.gameObject.name == "RacketLeft")
        {
            audioRacket.Play();
            float y = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);

            Vector2 dir = new Vector2(1, y).normalized;

            GetComponent<Rigidbody2D>().velocity = speed * Time.deltaTime * dir;
            lastHit = 0;
        }

        //right
        if (col.gameObject.name == "RacketRight")
        {
            audioRacket.Play();
            float y = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);

            Vector2 dir = new Vector2(-1, y).normalized;

            GetComponent<Rigidbody2D>().velocity = speed * Time.deltaTime * dir;
            lastHit = 1;
        }

        //Wall right e Wall left
        if (col.gameObject.name == "Right" || col.gameObject.name == "Left")
        {
            EndMatch();
        }

        //Wall-Top
        if (col.gameObject.name == "Top")
        {
            audioRacket.Play();
            Vector2 dir;

            if (lastHit == 0)
            {
                dir = new Vector2(1, -1).normalized;
            }
            else
            {
                dir = new Vector2(-1, -1).normalized;
            }

            GetComponent<Rigidbody2D>().velocity = speed * Time.deltaTime * dir;
        }

        //Wall-Bottom
        if (col.gameObject.name == "Bottom")
        {
            audioRacket.Play();
            Vector2 dir;

            if (lastHit == 0)
            {
                dir = new Vector2(1, 1).normalized;
            }
            else
            {
                 dir = new Vector2(-1, 1).normalized;
            }

            GetComponent<Rigidbody2D>().velocity = speed * Time.deltaTime * dir;
        }
    }

    void EndMatch()
    {
        audioWall.Play();
        GetComponent<Rigidbody2D>().position = Vector2.zero;
        StartMatch();
    }

    void StartMatch()
    {
        GetComponent<Rigidbody2D>().velocity = speed * Vector2.right;
    }
}
