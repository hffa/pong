﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Tooltip("Velocidade da raquete.")]
    float speed = 960;
    [Tooltip("bolinha")]
    public Transform ball;
    [Tooltip("direção do enemy")]
    Vector2 direction;

    void Update()
    {
        var heading = transform.position - ball.position;
        var distance = heading.magnitude;
        direction = heading / distance;

        GetComponent<Rigidbody2D>().AddForce(speed * Time.deltaTime * -direction);
    }
}
