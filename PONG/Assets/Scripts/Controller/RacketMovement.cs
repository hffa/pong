﻿using UnityEngine;
using System.Collections;

public class RacketMovement : MonoBehaviour
{
    [Tooltip("Velocidade da raquete.")]
    float speed = 560;
    float touchSpeed = 30;
    [Tooltip("Raquete.")]
    public string axis;

    void FixedUpdate()
    {
        if (!Application.isMobilePlatform)
        {
            float v = Input.GetAxisRaw(axis);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, v) * speed * Time.deltaTime;
        }
        else
        {
            if(Input.touchCount > 0)
            {
                float v = Input.touches[0].deltaPosition.y;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, v) * touchSpeed * Time.deltaTime;
            }
        }
    }
}